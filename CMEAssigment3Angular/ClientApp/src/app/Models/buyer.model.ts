export interface BuyerModel {
  id: Number,
  fullName: String,
  credit: Number,
  nbOfPurchased: Number
}
