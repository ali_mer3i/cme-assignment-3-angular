export interface AppartmentFilterModel {
  fromPrice: Number,
  toPrice: Number,
  address: String,
  nbOfRooms: Number
}
