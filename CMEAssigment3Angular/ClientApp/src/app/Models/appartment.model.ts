export interface AppartmentModel {
  id: Number,
  title: String,
  price: Number,
  address: String,
  nbOfRooms: Number
}
