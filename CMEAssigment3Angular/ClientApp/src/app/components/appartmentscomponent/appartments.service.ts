import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { AppartmentModel } from "../../Models/appartment.model";
import { AppartmentFilterModel } from "../../Models/appartmentFilter.model";
import { AlertComponent } from '../alert/alert.component';
import { MatDialog } from '@angular/material';
import { NBRooms } from '../../Models/nmRooms.model'

const APPARTMENT_URL = environment.backEndURL + "api/appartments";
const BUYER_URL = environment.backEndURL + "api/buyers";

@Injectable({ providedIn: "root" })
export class AppartmenstService {
  private appartments: AppartmentModel[] = [];
  private appartmentsUpdated = new Subject<AppartmentModel[]>();

  nbRoomsArray: NBRooms[] = [
    { value: 1, viewValue: '1' },
    { value: 2, viewValue: '2' },
    { value: 3, viewValue: '3' },
    { value: 4, viewValue: '4' },
    { value: 5, viewValue: '5' },
    { value: 6, viewValue: '6' },
    { value: 7, viewValue: '7' },
    { value: 8, viewValue: '8' },

  ];

  constructor(private http: HttpClient, private router: Router,
    private dialog: MatDialog) { }

  getAppartments() {

    this.http
      .get<{ message: string; appartments: any }>(APPARTMENT_URL)
      .subscribe(transformedAppartments => {
        this.appartments = transformedAppartments;
        this.appartmentsUpdated.next([...this.appartments]);

      });
  }

  getAppartmentsFiltered(appartmentFilter: AppartmentFilterModel) {

    this.http
      .post<{ message: string; appartment: AppartmentFilterModel }>(
        APPARTMENT_URL + "/filter",
        appartmentFilter
      )
      .subscribe(transformedAppartments => {
        this.appartments = transformedAppartments;
        this.appartmentsUpdated.next([...this.appartments]);

      });
  }

  getBuyers() {

    this.http
      .get<{ message: string; buyers: any }>(BUYER_URL)
      .subscribe(transformedBuyers => {
        console.log(transformedBuyers);
        this.appartments = transformedBuyers;
        this.appartmentsUpdated.next([...this.appartments]);

      });
  }
  getAppartmentUpdatedListener() {
    return this.appartmentsUpdated.asObservable();
  }

  addAppartment(appartmentData: AppartmentModel) {

    this.http
      .post<{ message: string; appartment: AppartmentModel }>(
        APPARTMENT_URL,
        appartmentData
      )
      .subscribe(responseData => {
        // const shipmentCarrier: AppartmentModel = responseData.shipmentCarriers
        // this.shipmentCarriers.push(shipmentCarrier);
        // this.shipmentCarriersUpdated.next([...this.shipmentCarriers]);
        this.router.navigate([""]);
      });
  }

  updateAppartment(
    id: Number,
    newaAppartment: any
  ) {
    let appartmentData;
    appartmentData = newaAppartment;

    this.http
      .put<{ message: string; appartment: AppartmentModel }>(
        APPARTMENT_URL + "/" + id,
        appartmentData
      )
      .subscribe(response => {
        this.router.navigate([""]);

      });
  }

  purchaseAppartment(appartmentID: Number, buyerID: Number) {

    this.http
      .get<{ message: string; appartments: any }>(APPARTMENT_URL + "/purchase/" + appartmentID + "/" + buyerID)
      .subscribe(transformedAppartments => {
        this.dialog.open(AlertComponent, { data: { title: "Successfull", message: "Purchase Done" } });
      });
  }

}
