import { Component } from '@angular/core';
import { AppartmentModel } from '../../Models/appartment.model'
import { NBRooms } from '../../Models/nmRooms.model'
import { AppartmenstService } from './appartments.service';
import { Subscription } from 'rxjs';
import { BuyerPurchaseComponent } from '../buyerPurchase/buyerPurchase.component';
import { MatDialog } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { AppartmentFilterModel } from "../../Models/appartmentFilter.model";

@Component({
  selector: 'app-maincomponent',
  templateUrl: './appartments.component.html',
  styleUrls: ['./appartments.component.css']
})

export class AppartmentsComponent {

  constructor(public appartmentsService: AppartmenstService,
    private dialog: MatDialog) { }

  private appartmentsSub: Subscription;
  filter: AppartmentFilterModel = { "address": "", "fromPrice": null, "toPrice": null, "nbOfRooms": null };
  displayedColumns = ["id", "title", "address", "rooms", "price", "purchase"];
  nbRooms: NBRooms[] = this.appartmentsService.nbRoomsArray;
  appartments: AppartmentModel[] = [];
  form: FormGroup;
  address: string;
  fromPrice: string;
  toPrice: string;
  selectedRooms: string;

  ngOnInit() {
    this.getAllAppartments();
  }

  getAllAppartments() {
    this.appartmentsService.getAppartments();
    this.appartmentsSub = this.appartmentsService
      .getAppartmentUpdatedListener()
      .subscribe((appartments: AppartmentModel[]) => {
        this.appartments = appartments
      });
  }

  onPurchase(appartmentID: Number) {
    this.dialog.open(BuyerPurchaseComponent, { data: { appartmentID: appartmentID } });

  }
  onSearch() {
    //assign address
    this.filter.address = (this.address) ? this.address : '';
    this.filter.fromPrice = (this.fromPrice) ? Number(this.fromPrice) : null;
    this.filter.toPrice = (this.toPrice) ? Number(this.toPrice) : null;
    this.filter.nbOfRooms = (this.selectedRooms) ? Number(this.selectedRooms) : null;

    this.appartmentsService.getAppartmentsFiltered(this.filter);
    this.appartmentsSub = this.appartmentsService
      .getAppartmentUpdatedListener()
      .subscribe((appartments: AppartmentModel[]) => {
        this.appartments = appartments
      });
  }
  onReset() {
    this.address =  '';
    this.fromPrice = '';
    this.toPrice = '';
    this.selectedRooms = '';
    this.getAllAppartments();
  }



}
