
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppartmentsComponent } from './appartments.component';

describe('AppartmentsComponent', () => {
  let component: AppartmentsComponent;
  let fixture: ComponentFixture<AppartmentsComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AppartmentsComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(AppartmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
