import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { AppartmentModel } from '../../../Models/appartment.model';
import { AppartmenstService } from '../appartments.service';
import { NBRooms } from '../../../Models/nmRooms.model'
import { Router } from "@angular/router";

@Component({
  selector: 'app-maincomponent',
  templateUrl: './appartmentDetailscomponent.component.html',
  styleUrls: ['./appartmenDetailscomponent.component.css']
})
export class AppartmentDetailsComponent implements OnInit {
  selectedRooms: Number;
  isLoading = false;
  private isCreate = true;
  private saveText = "Add Appartment"
  private appartmentId: Number;
  form: FormGroup;
  appartment: AppartmentModel;

  nbRooms: NBRooms[] = this.appartmentsService.nbRoomsArray;

  appartmentCreated = new EventEmitter<AppartmentModel>();

  constructor(public appartmentsService: AppartmenstService,
    public route: ActivatedRoute,
    private router: Router,) { }

  ngOnInit() {
    this.appartmentId = 0;
    this.form = new FormGroup({
      title: new FormControl(null, { validators: [Validators.required }),
      address: new FormControl(null, { validators: [Validators.required] }),
      price: new FormControl(null, { validators: [] }),
      rooms: new FormControl(null, { validators: [Validators.required] })

    });
    // in order to listen if link changed whle we still in the same component
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('appartmentID')) {
        this.isCreate = false;
        this.saveText = "Edit Appartment";
        this.appartmentId = parseInt(paramMap.get('appartmentID'));
        this.route
          .queryParams
          .subscribe(params => {
            this.form.controls["title"].setValue(params["title"]);
            this.form.controls["address"].setValue(params["address"]);
            this.form.controls["price"].setValue(params["price"]);
            this.form.controls["rooms"].setValue(parseInt(params["nbOfRooms"]));


          });

        this.isLoading = false;
      } else {
        this.isCreate = true;
        this.appartmentId = null;
      }
    });
  }
  onCancel() {
    this.router.navigate([""]);
  }
  onSavePost() {
    if (this.form.invalid) {
      return;
    }
    if (this.isCreate === true) {
      const app: AppartmentModel = {
        "id": 0,
        "title": this.form.value.title,
        "address": this.form.value.address,
        "nbOfRooms": this.form.value.rooms,
        "price": 0
      };
      this.appartmentsService.addAppartment(app);
    } else {
      const app: AppartmentModel = {
        "id": this.appartmentId,
        "title": this.form.value.title,
        "address": this.form.value.address,
        "nbOfRooms": this.form.value.rooms,
        "price": this.form.value.price
      };
      this.appartmentsService.updateAppartment(this.appartmentId, app);
    }
  }

}
