
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppartmentDetailsComponent } from './appartmentDetailscomponent.component';

describe('AppartmentDetailsComponent', () => {
  let component: AppartmentDetailsComponent;
  let fixture: ComponentFixture<AppartmentDetailsComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AppartmentDetailsComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(AppartmentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
