import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { BuyerModel } from "../../Models/buyer.model";
import { MatDialog } from '@angular/material';

const BUYER_URL = environment.backEndURL + "api/buyers";

@Injectable({ providedIn: "root" })
export class BuyersService {
  private buyers: BuyerModel[] = [];
  private appartmentsUpdated = new Subject<BuyerModel[]>();

  constructor(private http: HttpClient, private router: Router,
    private dialog: MatDialog) { }


  getBuyers() {

    this.http
      .get<{ message: string; buyers: any }>(BUYER_URL)
      .subscribe(transformedBuyers => {
        console.log(transformedBuyers);
        this.buyers = transformedBuyers;
        this.appartmentsUpdated.next([...this.buyers]);
        this.dialog.closeAll;
      });
  }
  getBuyersUpdatedListener() {
    return this.appartmentsUpdated.asObservable();
  }
  

 
}
