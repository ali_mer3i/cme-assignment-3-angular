import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { BuyerModel } from "../../Models/buyer.model";
import { Subscription } from 'rxjs';
import { BuyersService } from './buyers.service'
import { AppartmenstService } from '../../components/appartmentscomponent/appartments.service'

@Component({
  templateUrl: './buyerPurchase.component.html',
  styleUrls: ['./buyerPurchase.component.css']
})
export class BuyerPurchaseComponent {
  private selectedBuyer: Number;

  constructor(@Inject(MAT_DIALOG_DATA) public data: { appartmentID: Number },
    public buyersService: BuyersService,
    public appartmentsService: AppartmenstService) { }

  private appartmentsSub: Subscription;
  buyers: BuyerModel[] = [];


  ngOnInit() {
    this.buyersService.getBuyers();
    this.appartmentsSub = this.buyersService
      .getBuyersUpdatedListener()
      .subscribe((appartments: BuyerModel[]) => {
        this.buyers = appartments
        //  this.source.load(this.appartments);
      });

  }
  onPurchase() {
    this.appartmentsService.purchaseAppartment(this.data.appartmentID, this.selectedBuyer);
  }
}
