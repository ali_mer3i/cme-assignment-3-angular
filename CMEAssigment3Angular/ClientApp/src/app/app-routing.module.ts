import { NgModule } from '@angular/core';
import { RouterModule, Routes, RouterState } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

import { AppartmentsComponent } from './components/appartmentscomponent/appartments.component';
import { AppartmentDetailsComponent } from './components/appartmentscomponent/appartmentsDetailsComponent/appartmentDetailscomponent.component';

 
const routes: Routes = [
  { path: '', component: AppartmentsComponent , pathMatch: 'full' },
  { path: 'create', component: AppartmentDetailsComponent },
  { path: 'edit/:appartmentID', component: AppartmentDetailsComponent },

];
 
@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
