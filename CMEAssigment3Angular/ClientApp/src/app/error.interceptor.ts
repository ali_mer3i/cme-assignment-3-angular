import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AlertComponent } from '../app/components/alert/alert.component';

// this affects all outgoing http requests
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private dialog: MatDialog) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMessage = 'An unkown error occured!';
        let errorTitle = 'Error';

        if (error.error.message) {
          errorMessage = error.error.message;
        }
        if (error.error.title) {
          errorTitle = error.error.title;
        }
       //alert(errorMessage);
        this.dialog.open(AlertComponent, { data: { title: errorTitle, message: errorMessage } });
        return throwError(error);
      })
    );
  }
}

