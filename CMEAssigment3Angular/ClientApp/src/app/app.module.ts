import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import {
  MatDialogModule, MatPaginatorModule, MatSelectModule,MatProgressSpinnerModule, MatSortModule, MatTabsModule,MatProgressBarModule,MatTableModule,MatButtonModule, MatCheckboxModule, MatGridListModule, MatCardModule, MatMenuModule, MatIconModule, MatToolbarModule, MatSidenavModule, MatListModule,MatFormFieldModule,MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { AppRoutingModule }     from './app-routing.module';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppartmentsComponent } from './components/appartmentscomponent/appartments.component';
import { AppartmentDetailsComponent } from './components/appartmentscomponent/appartmentsDetailsComponent/appartmentDetailscomponent.component';
import { ErrorInterceptor } from './error.interceptor';
import { AlertComponent } from '../app/components/alert/alert.component';
import { BuyerPurchaseComponent } from '../app/components/buyerPurchase/buyerPurchase.component'
import { NBRoomsClass } from './Models/nmRooms.model'


@NgModule({
  declarations: [
    AppComponent,
    AppartmentsComponent,
    AppartmentDetailsComponent,
    AlertComponent,
    BuyerPurchaseComponent,

  ],
  entryComponents:[],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDialogModule, MatTabsModule, MatSelectModule,MatProgressBarModule,MatTableModule, MatInputModule , MatFormFieldModule, MatButtonModule,MatCheckboxModule, BrowserAnimationsModule, MatGridListModule, MatCardModule, MatMenuModule, MatIconModule, LayoutModule, MatToolbarModule, MatSidenavModule, MatListModule, AppRoutingModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    AppRoutingModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],

  entryComponents: [AlertComponent,
                    BuyerPurchaseComponent]

})
export class AppModule { }
